const { browserslist } = require('./package.json');

module.exports = {
    processors: ['stylelint-processor-html'],
    extends: 'stylelint-config-primer',
    rules: {
        indentation: 4,
        'plugin/no-unsupported-browser-features': [
            true,
            {
                severity: 'warning',
                browsers: browserslist,
                ignore: [
                    'flexbox',
                    'outline',
                    'object-fit',
                    'css-filters',
                    'multicolumn',
                    'transforms3d',
                    'viewport-units',
                    'css-appearance',
                    'css-mixblendmode',
                    'font-unicode-range',
                    'css3-cursors-newer',
                    'flexbox',
                    'outline',
                    'transforms3d'
                ],
            },
        ],
        'scss/at-rule-no-unknown': true,
    },
};
