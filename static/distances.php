<?php

use Symfony\Component\Yaml\Yaml;

require_once '../vendor/autoload.php';

$distances = Yaml::parse(file_get_contents(__DIR__.'/../src/yml/data/distances.yml'), true);

echo json_encode($distances);

die();

