<?php
use Symfony\Component\Yaml\Yaml;

require_once '../vendor/autoload.php';

$file = null;
$distance = intval($_GET['distance'] ?? null);
$latitude = floatval($_GET['latitude'] ?? null);
$longitude = floatval($_GET['longitude'] ?? null);

if ($latitude === 39.9556241 && $longitude === -75.1647529) {
    if ($distance === 5) {
        $file = 'philadelphia-10';
    }

    if ($distance === 10) {
        $file = 'philadelphia-10';
    }

    if ($distance === 20) {
        $file = 'philadelphia-10';
    }
}

if (!$file) {
    echo json_encode([]);

    die();
}

echo json_encode(Yaml::parse(file_get_contents(__DIR__.'/../src/yml/data/stores/'.$file.'.yml'), true));

die();

