# BIMB-171-00w

> A 160over90 project

## Setup

Attached are the working files for your project, **BIMB-171-00w**. **BIMB-171-00w** has been built with a combination of traditional [**HTML**](https://www.w3schools.com/htmL/), [**SCSS**](https://sass-lang.com/), and [**Vue.js**](https://vuejs.org/v2/guide/) for the prefered JavaScript framework. Your website has been componentized and templatized using [**Twig**](https://twig.symfony.com/) templating engine, with each working page rendered and configured with a corresponding [**YAML**](http://yaml.org/) file found in `src/yml`. As such, you will need to generate a new `.env` file based on the provided example and run the following terminal commands in the root of the provided project folder to access your developer environment and render build files.

For your `.env`, simply delete the .example from your `.env.example` filename and save.
Then access your terminal software of choice and run:


##### install JavaScript dependencies
`yarn install`

##### install php dependencies
`composer install`

##### build for production with minification.
`yarn run build`
- *You should run this at least once prior to running dev to render image files.*
- *Command must be ran after the addition of any image assets in the static folder*

##### serve with hot reload
`yarn run dev`

After running these commands, you should be able work properly with these files with your text editor of choice.

This was built on MAC OS using [**Laravel Valet**](https://laravel.com/docs/5.6/valet) as the local server. The server driver can be found in `LocalValetDriver.php` in your project root.

## Store Locator

Below are all the necessary file paths for the store locator. Before getting started, make sure to add your API key to the `.env`  `GOOGLE_MAPS_API_KEY` variable. [Click Here](https://developers.google.com/maps/documentation/javascript/get-api-key) to obtain a key.

### Store Locator API

Contains fake store data and populates `src/components/StoreResults.vue` e.g., map markers, products for store:

`src/yml/data/stores/philadelphia-10.yml`

Contains array of distances for dropdown in `src/twig/components/store-filter.twig`:

`static/distances.php`

PHP script for simulating an API call:

`static/stores.php`

### Store Locator Data

Populates `src/twig/components/store-filter.twig`:

`src/yml/data/stores/store-locator.yml`
