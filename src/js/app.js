// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import svg4everybody from 'svg4everybody';
import objectFitImages from 'object-fit-images';

import App from '@/components/App';
import intersect from '@/js/directives/intersect';

// Modal
import ModalButton from '@/components/modal/ModalButton';
import ModalBackdrop from '@/components/modal/ModalBackdrop';
import ModalBackdropDialog from '@/components/modal/ModalBackdropDialog';

// Disclosure
import DisclosureItem from '@/components/disclosure/DisclosureItem';
import DisclosureButton from '@/components/disclosure/DisclosureButton';
import DisclosureContainer from '@/components/disclosure/DisclosureContainer';

// Accordion
import AccordionContainer from '@/components/accordion/AccordionContainer';
import AccordionContainerPanel from '@/components/accordion/AccordionContainerPanel';
import AccordionContainerHeader from '@/components/accordion/AccordionContainerHeader';
import AccordionContainerHeaderButton from '@/components/accordion/AccordionContainerHeaderButton';

// Packages
import vueSmoothScroll from 'vue-smooth-scroll';
import VueTouch from 'vue-touch';
import Vuelidate from 'vuelidate';
import 'whatwg-fetch';

// Tabs
import TabsContainer from '@/components/tabs/TabsContainer';
import TabsContainerTablist from '@/components/tabs/TabsContainerTablist';
import TabsContainerTabpanel from '@/components/tabs/TabsContainerTabpanel';
import TabsContainerTablistTab from '@/components/tabs/TabsContainerTablistTab';

// Filter
import GlobalFilter from '@/components/filter/GlobalFilter';
import GlobalFilterButton from '@/components/filter/GlobalFilterButton';
import GlobalFilterItemsWrap from '@/components/filter/GlobalFilterItemsWrap';
import GlobalFilterItem from '@/components/filter/GlobalFilterItem';
import GlobalFilterItemGhost from '@/components/filter/GlobalFilterItemGhost';

// ChannelSight
import ChannelSight from '@/components/channel-sight/ChannelSight';
import ChannelSightList from '@/components/channel-sight/ChannelSightList';

// Coupon
import Coupon from '@/components/Coupon';

// Product Index
import ProductIndex from '@/components/product-index/ProductIndex';

// Product Grid
import ProductGridItem from '@/components/product-grid/ProductGridItem';

// Product Details
import ProductDetails from '@/components/product-details/ProductDetails';

// Transitions
import TransitionSlide from '@/components/transitions/TransitionSlide';

// Header
import GlobalHeader from '@/components/GlobalHeader';

// Form
import TheForm from '@/components/TheForm';

// Video Modal
import VideoModal from '@/components/VideoModal';

// Smooth scroll jump-link
import BaseJumpLink from '@/components/BaseJumpLink';

// Carousel
import CarouselItem from '@/components/BaseCarousel/CarouselItem';
import CarouselContainer from '@/components/BaseCarousel/CarouselContainer';

// Checkbox
import CheckBox from '@/components/CheckBox';

// Reset Button
import ResetFilter from '@/components/ResetFilter';

// Store locater
import Locator from '@/components/Locator';
import StoreInput from '@/components/StoreInput';
import StoreResults from '@/components/StoreResults';

// Fade Slider
import FadeSlider from '@/components/FadeSlider';

// Intersection
import intersection from '@/components/intersection';

// Intersection
import GlobalIntersectWrap from '@/components/GlobalIntersectWrap';

// Transition Fade
import TransitionFade from '@/components/TransitionFade';

Vue.config.productionTip = false;

Vue.use(VueTouch, {
    name: 'v-touch',
});

Vue.use(Vuelidate);

Vue.use(vueSmoothScroll);
Vue.directive('intersect', intersect);

// Modal
Vue.component('ModalButton', ModalButton);
Vue.component('ModalBackdrop', ModalBackdrop);
Vue.component('ModalBackdropDialog', ModalBackdropDialog);

// Disclosure
Vue.component('DisclosureItem', DisclosureItem);
Vue.component('DisclosureButton', DisclosureButton);
Vue.component('DisclosureContainer', DisclosureContainer);

// Accordion
Vue.component('AccordionContainer', AccordionContainer);
Vue.component('AccordionContainerPanel', AccordionContainerPanel);
Vue.component('AccordionContainerHeader', AccordionContainerHeader);
Vue.component('AccordionContainerHeaderButton', AccordionContainerHeaderButton);

// Tabs
Vue.component('TabsContainer', TabsContainer);
Vue.component('TabsContainerTablist', TabsContainerTablist);
Vue.component('TabsContainerTabpanel', TabsContainerTabpanel);
Vue.component('TabsContainerTablistTab', TabsContainerTablistTab);

// Filter
Vue.component('GlobalFilter', GlobalFilter);
Vue.component('GlobalFilterButton', GlobalFilterButton);
Vue.component('GlobalFilterItemsWrap', GlobalFilterItemsWrap);
Vue.component('GlobalFilterItem', GlobalFilterItem);
Vue.component('GlobalFilterItemGhost', GlobalFilterItemGhost);

// ChannelSight
Vue.component('ChannelSight', ChannelSight);
Vue.component('ChannelSightList', ChannelSightList);

// Coupon
Vue.component('Coupon', Coupon);

// Transition Fade
Vue.component('TransitionFade', TransitionFade);

// Product Index
Vue.component('ProductIndex', ProductIndex);

// Product Grid
Vue.component('ProductGridItem', ProductGridItem);

// Product Details
Vue.component('ProductDetails', ProductDetails);

// Transitions
Vue.component('TransitionSlide', TransitionSlide);

// Header
Vue.component('GlobalHeader', GlobalHeader);

// Checkbox
Vue.component('CheckBox', CheckBox);

// Reset Filter
Vue.component('ResetFilter', ResetFilter);

// Form
Vue.component('TheForm', TheForm);
// Video Modal;
Vue.component('VideoModal', VideoModal);

// Smooth scroll jump-link
Vue.component('BaseJumpLink', BaseJumpLink);

// Store Locator
Vue.component('Locator', Locator);
Vue.component('StoreInput', StoreInput);
Vue.component('StoreResults', StoreResults);

// Carousel
Vue.component('CarouselItem', CarouselItem);
Vue.component('CarouselContainer', CarouselContainer);

// Fade Slider
Vue.component('FadeSlider', FadeSlider);

// Intersection
Vue.component('intersection', intersection);

// Global Intersect Wrap
Vue.component('GlobalIntersectWrap', GlobalIntersectWrap);


/* eslint-disable no-new */
new Vue(App).$mount('#app');

svg4everybody({
    validate(src) {
        return !src.startsWith('#plyr-');
    },
});
objectFitImages();
