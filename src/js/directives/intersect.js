let intersectionObserver;

export default {
    bind(el, { value, arg, modifiers }) {
        const options = Object.assign({
            threshold: 0.5,
        }, value);
        let hasEntered = false;

        intersectionObserver = new IntersectionObserver(
            entries => entries.forEach(({ target, intersectionRatio }) => {
                const isEntering = intersectionRatio >= options.threshold;

                if (!modifiers.replay && hasEntered) {
                    return;
                }

                target.classList[isEntering ? 'add' : 'remove'](arg || 'appear');

                if (!hasEntered && isEntering) {
                    hasEntered = true;
                }
            }),
            options,
        );

        intersectionObserver.observe(el);
    },
};
