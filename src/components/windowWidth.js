export default {
    data() {
        return {
            windowWidth: undefined,
        };
    },
    mounted() {
        const { getWindowWidth } = this;

        this.$nextTick(() => {
            window.addEventListener('resize', getWindowWidth);
            getWindowWidth();
        });
    },
    beforeDestroy() {
        window.removeEventListener('resize', this.getWindowWidth);
    },
    methods: {
        getWindowWidth() {
            this.windowWidth = document.documentElement.clientWidth;
        },
    },
};

